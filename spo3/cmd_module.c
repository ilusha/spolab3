#include "header/cmd_module.h"
#include "consts.c"
#include "header/utils.h"
#include "header/ui_module.h"
#include "header/global_structures.h"
#include "header/udp_module.h"


void build_file_description(struct file *file, char *file_description)
{
    strcat(file_description, file->name);
    strcat(file_description, "/");
    char buffer[MAX_ARG_LENGTH];
    sprintf(buffer, "%d", file->size);
    strcat(file_description, buffer);
    strcat(file_description, "/");
    strcat(file_description, file->md5_hash);
}


int execute_find(struct global_structure *global_structure, char *relative_file_path, char *file_description)
{
    if (!strcmp("", relative_file_path))
    {
        print_(global_structure->windows_state,
               "You must specify relative path to file in current workspace as parameters of 'find' command", 1);
        return 2;
    }

    if (relative_file_path[0] == '/')
    {
        print_(global_structure->windows_state, "Specify file path as relative path", 1);
        return 2;
    }

    struct windows_state *windows_state = global_structure-> windows_state;
    GList *current_element = global_structure->files_list;

    int file_found = 0;

    while(current_element != NULL)
    {
        struct file *file = (struct file *) current_element->data;

        if (!strcmp(file->relative_path, relative_file_path))
        {
            build_file_description(file, file_description);

            file_found = 1;

            break;
        }

        current_element = current_element->next;
    }

    if (!file_found)
    {
        return 1;
    }

    return 0;
}


struct file *has_file_description(struct global_structure *global_structure, char *target_file_description)
{
    struct windows_state *windows_state = global_structure-> windows_state;
    GList *current_element = global_structure->files_list;

    int description_found = 0;

    while(current_element != NULL)
    {
        char *current_file_description = calloc(1, MAX_FILE_DESCRIPTION_LENGTH);
        struct file *file = (struct file *) current_element->data;

        build_file_description(file, current_file_description);

        if (!strcmp(current_file_description, target_file_description))
        {
            description_found = 1;

            free(current_file_description);

            return file;
        }

        free(current_file_description);

        current_element = current_element->next;
    }

    return NULL;
}


void execute_download(struct global_structure *global_structure, char *target_file_description)
{
    if (!strcmp("", target_file_description))
    {
        print_(global_structure->windows_state, "U should pu file_name/size/hash ", 1);
        return;
    }

    find_and_download(global_structure, target_file_description);
}


void run_command_line(struct global_structure *global_structure)
{
    struct windows_state *windows_state = global_structure->windows_state;

    while (!global_structure->close_program)
    {
        char *command = calloc(1, MAX_COMMAND_LENGTH);
        create_command_line_window(windows_state->windows->command_line_window);
        curs_set(1);
        wgetstr(windows_state->windows->command_line_window, command);

        curs_set(0);

        char* args[MAX_ARGS];
        for (int i = 0; i < MAX_ARGS; i++)
        {
            args[i] = calloc(1, MAX_ARG_LENGTH);
        }

        parse_args(command, args, MAX_ARGS);

        if (!strcmp("find", args[0]))
        {
            normalize_path(args[1]);
            char *file_description = calloc(1, MAX_FILE_DESCRIPTION_LENGTH);
            int result = execute_find(global_structure, args[1], file_description);

            if (result == 0)
            {
                print_(windows_state, file_description, 1);
            }
            else if (result == 1)
            {
                print_(windows_state, "File not found", 1);
            }

            free(file_description);
        }
        else if (!strcmp("download", args[0]))
        {
            int result = (has_file_description(global_structure, args[1]) != NULL);

            if (result == 1)
            {
                print_(windows_state, "This file already here", 1);
            }
            else if (result == 0)
            {
                execute_download(global_structure, args[1]);
            }
        }
        else if (!strcmp("exit", args[0]))
        {
            global_structure->close_program = 1;
        }
        else if (!strcmp("help", args[0]))
        {
            print_(windows_state, "Existing commands:", 1);
            print_(windows_state, " find - find file by relative path", 0);
            print_(windows_state, " download - download file_name/size/hash", 0);
            print_(windows_state, " exit - close programm", 0);
            print_(windows_state, " help - :)", 0);
        }
        else if (!strcmp("", args[0]))
        {
            // empty line. Do nothing
        }
        else
        {
            print_(windows_state, "Wrong command. U can use \"help\" for all possible commands", 1);
        }

        for (int i = 0; i < MAX_ARGS; i++)
        {
            free(args[i]);
        }

        free(command);
    }
}
