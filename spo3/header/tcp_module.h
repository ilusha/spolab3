#include "global_structures.h"
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include "../header/ui_module.h"
#include "../consts.c"
#include "../header/cmd_module.h"
#include "../header/utils.h"




void take_tcp_port(struct global_structure *global_structure, struct tcp_port_description *tcp_port_description);

void create_tcp_listener(struct tcp_listener_thread_struct *tcp_listener_thread_struct);

void add_tcp_client(struct tcp_client_thread_struct *tcp_client_thread_struct);
