#include "global_structures.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include "../header/ui_module.h"
#include "../consts.c"
#include "../header/cmd_module.h"
#include "../header/tcp_module.h"
#include "../header/utils.h"

void create_udp_listener(struct global_structure *global_structure);

void find_and_download(struct global_structure *global_structure, char *file_description);
