#include "./global_structures.h"

void create_header_windows(WINDOW *header_window1, WINDOW *header_window2);

void create_downloading_window(struct windows_state *windows_state);

void create_uploading_window(struct windows_state *windows_state);

void create_log_window(struct windows_state *windows_state);

void create_command_line_window(WINDOW *command_line_window);

void print_(struct windows_state *windows_state, char *string, char show_time);

void create_ui(struct global_structure *global_structure);

void destroy_ui(struct windows_state *windows_state);

void update_downloading_file(struct windows_state *windows_state, struct downloading_file *downloading_file);

void update_uploading_file(struct windows_state *windows_state, struct uploading_file *uploading_file);
