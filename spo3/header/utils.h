#include <glib.h>
#include "global_structures.h"
#include "../consts.c"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <openssl/md5.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

void parse_args(const char *command, char *args[], int args_number);

void normalize_path(char *normalized_path);

struct file *description_to_file(char *file_description);

void process_dir(char *root_path, char *dir_path, struct files_list_wrapper *files_list_wrapper);

int get_shared_files(char *working_dir_path, struct files_list_wrapper *files_list_wrapper);

void free_file(struct file *file);

int get_file_size(char *file_path);
