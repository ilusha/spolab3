#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
//#include <bits/pthreadtypes.h>
#include <pthread.h>
#include "header/global_structures.h"
#include "header/ui_module.h"
#include "header/udp_module.h"
#include "header/cmd_module.h"
#include "header/utils.h"


int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("You should also put a working directory\n");
    }
    else if (argc == 2)
    {
        struct files_list_wrapper *files_list_wrapper = calloc(1, sizeof(struct files_list_wrapper));
        files_list_wrapper->files_list = NULL;

        if (get_shared_files(argv[1], files_list_wrapper))
        {
            printf("Couldn't read the directory\n");
            return 1;
        }

        struct global_structure *global_structure = calloc(1, sizeof(struct global_structure));
        global_structure->root_path = argv[1];
        global_structure->files_list = files_list_wrapper->files_list;
        global_structure->close_program = 0;
        free(files_list_wrapper);

        create_ui(global_structure);

        pthread_t *udp_listener_thread = malloc(sizeof(pthread_t));
        pthread_create(udp_listener_thread, NULL, (void *) create_udp_listener, global_structure);

        create_header_windows(global_structure->windows_state->windows->header_window1,
                              global_structure->windows_state->windows->header_window2);
        create_downloading_window(global_structure->windows_state);
        create_uploading_window(global_structure->windows_state);
        create_log_window(global_structure->windows_state);

        run_command_line(global_structure);

        pthread_cancel(*udp_listener_thread);
        pthread_join(*udp_listener_thread, NULL);

        free_context(global_structure);

        free(udp_listener_thread);
    }
    else
    {
        printf("To many arguments\n");
    }

    return 0;
}
